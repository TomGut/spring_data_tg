package pl.cm;

import static java.lang.System.out;

import java.util.List;
import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;

import pl.cm.model.User;
import pl.cm.model.UserBuilder;
import pl.cm.repository.SimpleUserRepository;

public class App {

  public static void main(String[] args){
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-configuration.xml");
    final SimpleUserRepository userRepository = context.getBean(SimpleUserRepository.class);

    out.println("Find by username optional result");
    final Optional<User> optionalByUsername = userRepository.findByUsername("itzel.cassin");
    out.println(optionalByUsername);

    out.println("Find by username ");
    final User byUsername = userRepository.findByTheUsersName("itzel.cassin");
    out.println(byUsername);

    out.println("Find by lastName");
    final List<User> byLastname = userRepository.findByLastname("White");
    out.println(byLastname);

    out.println("Remove");
    final Long removedCount = userRepository.removeByLastname("MacGyver");
    out.println(removedCount);

    out.println("Pageable");
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(0,2)));
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(1,2)));
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(2,2)));

    out.println("Find first 2 with order");
    final List<User> first2ByOrderByLastnameAsc = userRepository.findFirst2ByOrderByLastnameAsc();
    out.println(first2ByOrderByLastnameAsc);

    out.println("Find first 2 with order");
    final List<User> byFirstnameOrLastname = userRepository.findByFirstnameOrLastname("White");
    out.println(byFirstnameOrLastname);

    out.println("Find first 2 with order SpEl");
    final User user = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byFirstnameOrLastnameSpEl = userRepository.findByFirstnameOrLastname(user);
    out.println(byFirstnameOrLastnameSpEl);


    out.println("Find By example firsname");
    final User userByExample = UserBuilder.anUser().withFirstname("White").build();
    final Iterable<User> byExampleFirstName = userRepository.findAll(Example.of(userByExample));
    out.println(byExampleFirstName);

    out.println("Find By example firsname and lastname");
    final User userByExampleAnd = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byExampleAnd = userRepository.findAll(Example.of(userByExampleAnd));
    out.println(byExampleAnd);

    out.println("Find By example firsname or lastname");
    final User userByExampleOr = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byExampleOr = userRepository.findAll(Example.of(userByExampleOr, ExampleMatcher.matchingAny()));
    out.println(byExampleOr);

  }
}
